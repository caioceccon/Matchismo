//
//  main.m
//  Matchismo
//
//  Created by Caio Ceccon on 6/12/13.
//  Copyright (c) 2013 Caio Ceccon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
